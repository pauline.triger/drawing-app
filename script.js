const canvas = document.getElementById('ecran')
const ctx = canvas.getContext('2d')
let coord ={x:0, y:0}
var tableauCoor = []
const buttonColor = document.getElementById('color-picker')
const buttonThickness = document.getElementById('thickness')
const checkboxElement = document.getElementById('checkbox')
var today = Date.now()

function displayDrawings(){
    $.ajax({
        url : 'https://api.draw.codecolliders.dev/paths',
        method: 'POST',
        id : 1,
        path : {
            0 : 0,
            1 : 1,
        }
    }).done(function(data){
        for (let i = 0; i < data.length; i++) {
            ctx.beginPath()
            ctx.strokeStyle = data[i].strokeColor
            ctx.lineWidth = data[i].lineWidth
                ctx.moveTo(data[i].path[0][0], data[i].path[0][1])
                for (let j = 0; j < data[i].path.length; j++) {
                    ctx.lineTo(data[i].path[j][0], data[i].path[j][1])
                    ctx.stroke()
                }
        }

    })
}


document.addEventListener('mousedown',startDrawing)

function coordMouse(event){
    coord.x = event.clientX
    coord.y = event.clientY
}

function startDrawing(){
    document.addEventListener('mousemove',draw)
    coordMouse(event)
}

function draw() {
    buttonColor.addEventListener('click', function(){})
    buttonThickness.addEventListener('click', function(){})
    ctx.strokeStyle = buttonColor.value
    ctx.lineWidth = buttonThickness.value
    tableauCoor.push([coord.x,coord.y])
    ctx.beginPath()
    ctx.moveTo(coord.x, coord.y)
    coordMouse(event)
    ctx.lineTo(coord.x, coord.y)
    ctx.stroke()
}

document.addEventListener('mouseup',stopDrawing)
function stopDrawing(){
    $.ajax({
        method : 'POST',
        url : 'https://api.draw.codecolliders.dev/paths/add',
        data : {
            path : tableauCoor,
            lineWidth : buttonThickness.value,
            strokeColor : buttonColor.value,
        }
    }).done(function(data){
})
    tableauCoor=[]
    document.removeEventListener('mousemove', draw)
}

function removeOldDrawings() {
    checkboxElement.addEventListener('change', function (check) {
        $.ajax({
            url: 'https://api.draw.codecolliders.dev/paths',
            method: 'POST',
            id: 1,
            path: {
                0: 0,
                1: 1,
            }
        }).done(function (data) {
            ctx.fillStyle = "white"
            ctx.fillRect(0,0 ,1000,500)
            for (let i = 0; i < data.length; i++) {
                if (checkboxElement.checked){
                    if (Date.parse(data[i].createdAt) > (today - 600000)) {
                        ctx.beginPath()
                        ctx.strokeStyle = data[i].strokeColor
                        ctx.lineWidth = data[i].lineWidth
                        ctx.moveTo(data[i].path[0][0], data[i].path[0][1])
                        for (let j = 0; j < data[i].path.length; j++) {
                            ctx.lineTo(data[i].path[j][0], data[i].path[j][1])
                            ctx.stroke()
                    }

                }}
                else{
                    setInterval(displayDrawings,1000)
                }
            }
        })
    })
}

setInterval(displayDrawings,5000)
removeOldDrawings()


function saveImage() {

    var imageData = canvas.toDataURL()
    var tmpLink = document.createElement( 'a' );
    tmpLink.download = 'image.png'; // set the name of the download file
    tmpLink.href = imageData;

    document.body.appendChild( tmpLink );
    tmpLink.click();
    document.body.removeChild( tmpLink );
}
const saveImageButton = document.getElementById('save-image')
saveImageButton.addEventListener('click', saveImage)